namespace BlogPlus.Authorization.Application.Defaults;

internal static class EmailDefaults
{
    internal const string FromName = "BlogPlus";
    
    internal const string FromAddress = "Support@blogplus.com";
    
    internal const string Host = "smtp.mailgun.org";
}