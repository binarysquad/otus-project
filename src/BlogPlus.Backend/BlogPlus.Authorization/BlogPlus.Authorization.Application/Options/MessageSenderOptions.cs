using System.ComponentModel.DataAnnotations;

namespace BlogPlus.Authorization.Application.Options;

public sealed record MessageSenderOptions
{
    [Required]
    public string UserName { get; init; }
    
    [Required]
    public string ApiKey { get; init; }
}