using BlogPlus.Authorization.Application.Models;
using Microsoft.AspNetCore.Identity;

namespace BlogPlus.Authorization.Application.Services;

public interface IJwtManager
{
    GeneratedJwtDto GenerateJwtToken(IdentityUser user);
}