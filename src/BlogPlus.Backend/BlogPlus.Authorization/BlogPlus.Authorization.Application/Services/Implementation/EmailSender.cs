using BlogPlus.Authorization.Application.Defaults;
using BlogPlus.Authorization.Application.Options;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace BlogPlus.Authorization.Application.Services.Implementation;

public sealed class EmailSender : IEmailSender
{
    private readonly IOptions<MessageSenderOptions> _messageSenderOptions;

    public EmailSender(IOptions<MessageSenderOptions> messageSenderOptions)
    {
        _messageSenderOptions = messageSenderOptions;
    }

    public async Task SendEmailAsync(string email, string subject, string message)
    {
        var mail = new MimeMessage ();
        mail.From.Add (new MailboxAddress (EmailDefaults.FromName, EmailDefaults.FromAddress));
        mail.To.Add (new MailboxAddress (email, email));
        mail.Subject = subject;
        
        var bodyBuilder = new BodyBuilder
        {
            HtmlBody = message
        };

        mail.Body = bodyBuilder.ToMessageBody();

        using var client = new SmtpClient();
        
        client.ServerCertificateValidationCallback = (_, c, _, _) => true;

        await client.ConnectAsync(EmailDefaults.Host, 587, false);
        client.AuthenticationMechanisms.Remove ("XOAUTH2");
        await client.AuthenticateAsync(_messageSenderOptions.Value.UserName, _messageSenderOptions.Value.ApiKey);

        await client.SendAsync(mail);
        await client.DisconnectAsync(true);
    }
}