using System.IdentityModel.Tokens.Jwt;
using System.Text;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Common.Authorization.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BlogPlus.Authorization.Application.Services.Implementation;

public class JwtManager : IJwtManager
{
    private readonly IOptions<JwtOptions> _jwtOptions;

    public JwtManager(IOptions<JwtOptions> jwtOptions)
    {
        _jwtOptions = jwtOptions;
    }

    public GeneratedJwtDto GenerateJwtToken(IdentityUser user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.UTF8.GetBytes(_jwtOptions.Value.Key);
        
        var issuedDateTime = DateTimeOffset.UtcNow;
        var expiredDateTime = issuedDateTime.Add(_jwtOptions.Value.ExpiredPeriod);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            IssuedAt = issuedDateTime.DateTime,
            Issuer = _jwtOptions.Value.Issuer,
            Audience = _jwtOptions.Value.Audience,
            Expires = expiredDateTime.DateTime,
            
            Claims = new Dictionary<string, object>
            {
                { JwtRegisteredClaimNames.Sub, user.Id },
                { JwtRegisteredClaimNames.Email, user.Email }, 
                { JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString() }
            },
                
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
        };

        var token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
        var jwtToken = tokenHandler.WriteToken(token);
        
        return new GeneratedJwtDto(jwtToken, expiredDateTime.ToUnixTimeSeconds());
    }
}