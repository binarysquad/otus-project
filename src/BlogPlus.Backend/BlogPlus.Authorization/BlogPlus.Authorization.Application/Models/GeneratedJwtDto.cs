namespace BlogPlus.Authorization.Application.Models;

public sealed record GeneratedJwtDto(string AccessToken, long Expires);