using System.Text.Json.Serialization;

namespace BlogPlus.Authorization.Application.Models;

public sealed record LoginOutputDto([property: JsonPropertyName("access_token")] string AccessToken, long Expires);