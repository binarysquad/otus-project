using System.Net;

namespace BlogPlus.Authorization.Application.Models;

public sealed record RegisterOutputDto(HttpStatusCode StatusCode);