using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Validators.SetPassword;

public sealed class SetPasswordCommandValidator : IPipelineBehavior<SetPasswordCommand, Task>
{
    private readonly IValidator<SetPasswordCommand> _validator;
    private readonly UserManager<User> _userManager;

    public SetPasswordCommandValidator(IValidator<SetPasswordCommand> validator, UserManager<User> userManager)
    {
        _validator = validator;
        _userManager = userManager;
    }

    public async ValueTask<Task> Handle(SetPasswordCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<SetPasswordCommand, Task> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage,validationResult.ToDictionary());
        
        var doesUserExist = await _userManager.Users.AnyAsync(u => u.Id == message.UserId, cancellationToken: cancellationToken);
        if (!doesUserExist)
            throw new NotFoundException(AuthorizationResources.UserNotFound);
        
        return await next(message, cancellationToken);
    }
}