﻿using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.SetPassword;

public sealed class SetPasswordModelValidator : AbstractValidator<SetPasswordCommand> 
{
    public SetPasswordModelValidator() 
    {
        RuleFor(p => p.UserId)
            .NotEmpty().WithMessage("Invalid access token.");
        
        RuleFor(p => p.Password)
            .IsPassword();
        
        RuleFor(x => x.ConfirmPassword)
            .NotEmpty().WithMessage("Your password cannot be empty.")
            .Equal(p => p.Password).WithMessage("Passwords should be equal.");
    }
}