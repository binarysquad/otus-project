using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Validators.Register;

public sealed class RegisterCommandValidator : IPipelineBehavior<RegisterUserCommand, RegisterOutputDto>
{
    private readonly IValidator<RegisterUserCommand> _validator;
    private readonly UserManager<User> _userManager;

    public RegisterCommandValidator(IValidator<RegisterUserCommand> validator, UserManager<User> userManager)
    {
        _validator = validator;
        _userManager = userManager;
    }

    public async ValueTask<RegisterOutputDto> Handle(RegisterUserCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<RegisterUserCommand, RegisterOutputDto> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage, validationResult.ToDictionary());

        var doesEmailExist = await _userManager.Users.AnyAsync(u => u.Email == message.Email, cancellationToken);
        if (doesEmailExist)
            throw new AlreadyExistsException(AuthorizationResources.UserAlreadyExists);

        return await next(message, cancellationToken);
    }
}