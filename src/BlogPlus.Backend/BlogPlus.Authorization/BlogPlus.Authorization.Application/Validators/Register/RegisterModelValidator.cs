using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.Register;

public sealed class RegisterModelValidator : AbstractValidator<RegisterUserCommand> 
{
    public RegisterModelValidator() 
    {
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Your email cannot be empty.")
            .EmailAddress().WithMessage("Your email is incorrect.");
        
        RuleFor(p => p.Password)
            .IsPassword();
    }
}