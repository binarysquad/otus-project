using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.Login;

public sealed class LoginModelValidator : AbstractValidator<LoginUserCommand> 
{
    public LoginModelValidator() 
    {
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Your email cannot be empty.")
            .EmailAddress().WithMessage("Your email is incorrect.");
    }
}