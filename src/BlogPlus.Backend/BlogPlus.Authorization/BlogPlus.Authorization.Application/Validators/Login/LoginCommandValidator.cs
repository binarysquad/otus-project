﻿using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Validators.Login;

public sealed class LoginCommandValidator : IPipelineBehavior<LoginUserCommand, LoginOutputDto>
{
    private readonly IValidator<LoginUserCommand> _validator;
    private readonly UserManager<User> _userManager;

    public LoginCommandValidator(IValidator<LoginUserCommand> validator, UserManager<User> userManager)
    {
        _validator = validator;
        _userManager = userManager;
    }

    public async ValueTask<LoginOutputDto> Handle(LoginUserCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<LoginUserCommand, LoginOutputDto> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage,validationResult.ToDictionary());
        
        var user = await _userManager.Users
            .Where(u => u.Email == message.Email)
            .Select(u => new
            {
                IsConfirmed = u.EmailConfirmed,
                LockoutEndDate = u.LockoutEnd
            })
            .FirstOrDefaultAsync(cancellationToken);
        
        if (user is null)
            throw new UserLockedException(AuthorizationResources.InvalidPassword);
        
        if (!user.IsConfirmed)
            throw new ValidationModelException(AuthorizationResources.EmailNotConfirmed);
        
        var isLocked = user.LockoutEndDate.HasValue && user.LockoutEndDate.Value >= DateTimeOffset.UtcNow;
        if (isLocked)
            throw new UserLockedException(AuthorizationResources.UserLocked);
        
        return await next(message, cancellationToken);
    }
}