using System.Text;
using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BlogPlus.Authorization.Application.Validators.Reset;

public sealed class ResetCommandValidator : IPipelineBehavior<ResetPasswordCommand, Task>
{
    private readonly IValidator<ResetPasswordCommand> _validator;
    private readonly UserManager<User> _userManager;
    private readonly string _passwordResetProvider;
    private readonly string _purpose = UserManager<User>.ResetPasswordTokenPurpose;

    public ResetCommandValidator(IValidator<ResetPasswordCommand> validator, UserManager<User> userManager, IOptions<IdentityOptions> identityOptions)
    {
        _validator = validator;
        _userManager = userManager;
        _passwordResetProvider = identityOptions.Value.Tokens.PasswordResetTokenProvider;
    }

    public async ValueTask<Task> Handle(ResetPasswordCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<ResetPasswordCommand, Task> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage,validationResult.ToDictionary());
        
        var user = await _userManager.Users
            .AsNoTracking()
            .SingleOrDefaultAsync(u => u.Email == message.Email, cancellationToken: cancellationToken);
        
        if (user is null)
            throw new NotFoundException(AuthorizationResources.UserNotFound);

        message.User = user;

        var decodedToken = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(message.Code));
        var isValid = await _userManager.VerifyUserTokenAsync(user, _passwordResetProvider, _purpose, decodedToken);
        if (!isValid)
            throw new ValidationModelException(AuthorizationResources.InvalidConfirmationCode);
        
        return await next(message, cancellationToken);
    }
}