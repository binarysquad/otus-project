using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.Reset;

public sealed class ResetPasswordModelValidator : AbstractValidator<ResetPasswordCommand> 
{
    public ResetPasswordModelValidator() 
    {
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Your email cannot be empty.")
            .EmailAddress().WithMessage("Your email is incorrect.");
        
        RuleFor(x => x.Code)
            .NotEmpty().WithMessage("Confirmation code cannot be empty.");

        RuleFor(p => p.Password)
            .IsPassword();
        
        RuleFor(x => x.ConfirmPassword)
            .NotEmpty().WithMessage("Your password cannot be empty.")
            .Equal(p => p.Password).WithMessage("Passwords should be equal.");
    }
}