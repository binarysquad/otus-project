using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Validators.Forgot;

public sealed class ForgotCommandValidator : IPipelineBehavior<ForgotPasswordCommand, Task>
{
    private readonly IValidator<ForgotPasswordCommand> _validator;
    private readonly UserManager<User> _userManager;

    public ForgotCommandValidator(IValidator<ForgotPasswordCommand> validator, UserManager<User> userManager)
    {
        _validator = validator;
        _userManager = userManager;
    }

    public async ValueTask<Task> Handle(ForgotPasswordCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<ForgotPasswordCommand, Task> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage,validationResult.ToDictionary());
        
        var doesEmailExist = await _userManager.Users.AnyAsync(u => u.Email == message.Email, cancellationToken);
        if (!doesEmailExist)
            throw new NotFoundException(AuthorizationResources.UserNotFound);

        return await next(message, cancellationToken);
    }
}