using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.Forgot;

public sealed class ForgotPasswordModelValidator : AbstractValidator<ForgotPasswordCommand> 
{
    public ForgotPasswordModelValidator() 
    {
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Your email cannot be empty.")
            .EmailAddress().WithMessage("Your email is incorrect.");
    }
}