using System.Text;
using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using FluentValidation;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BlogPlus.Authorization.Application.Validators.Confirm;

public sealed class ConfirmCommandValidator : IPipelineBehavior<ConfirmUserCommand, Task>
{
    private readonly IValidator<ConfirmUserCommand> _validator;
    private readonly UserManager<User> _userManager;
    private readonly string _emailConfirmationProvider;
    private readonly string _purpose = UserManager<User>.ConfirmEmailTokenPurpose;

    public ConfirmCommandValidator(IValidator<ConfirmUserCommand> validator, UserManager<User> userManager, IOptions<IdentityOptions> identityOptions)
    {
        _validator = validator;
        _userManager = userManager;
        _emailConfirmationProvider = identityOptions.Value.Tokens.EmailConfirmationTokenProvider;
    }

    public async ValueTask<Task> Handle(ConfirmUserCommand message, CancellationToken cancellationToken, MessageHandlerDelegate<ConfirmUserCommand, Task> next)
    {
        var validationResult = await _validator.ValidateAsync(message, cancellationToken);
        if (!validationResult.IsValid)
            throw new ValidationModelException(AuthorizationResources.ValidationErrorMessage,validationResult.ToDictionary());

        var user = await _userManager.Users
            .AsNoTracking()
            .SingleOrDefaultAsync(u => u.Id == message.UserId, cancellationToken: cancellationToken);
        
        if (user is null)
            throw new NotFoundException(AuthorizationResources.UserNotFound);
        
        if (user.EmailConfirmed)
            throw new ValidationModelException(AuthorizationResources.AlreadyConfirmed);

        var decodedToken = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(message.Code));
        var isValid = await _userManager.VerifyUserTokenAsync(user, _emailConfirmationProvider, _purpose, decodedToken);
        if (!isValid)
            throw new ValidationModelException(AuthorizationResources.InvalidConfirmationCode);
        
        return await next(message, cancellationToken);
    }
}