using BlogPlus.Authorization.Application.Commands;
using FluentValidation;

namespace BlogPlus.Authorization.Application.Validators.Confirm;

public sealed class ConfirmModelValidator : AbstractValidator<ConfirmUserCommand> 
{
    public ConfirmModelValidator() 
    {
        RuleFor(x => x.Code)
            .NotEmpty().WithMessage("Confirmation code cannot be empty");
        
        RuleFor(p => p.UserId)
            .NotEmpty().WithMessage("Your password cannot be empty")
            .Must(p => Guid.TryParse(p, out _)).WithMessage("Invalid user identifier.");
    }
}