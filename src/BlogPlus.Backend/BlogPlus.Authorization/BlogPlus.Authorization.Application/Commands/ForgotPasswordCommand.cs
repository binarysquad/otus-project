using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record ForgotPasswordCommand(string Email) : ICommand<Task>;