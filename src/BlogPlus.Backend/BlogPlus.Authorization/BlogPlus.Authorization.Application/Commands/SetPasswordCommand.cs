﻿using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record SetPasswordCommand(string Password, string ConfirmPassword, string UserId) : ICommand<Task>;