using BlogPlus.Authorization.Domain.Models;
using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record ResetPasswordCommand(string Email, string Password, string ConfirmPassword, string Code, User User) : ICommand<Task>
{
    public User User { get; set; } = User;
}