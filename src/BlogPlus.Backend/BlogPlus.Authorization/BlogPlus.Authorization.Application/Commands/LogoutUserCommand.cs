using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record LogoutUserCommand(string AccessToken) : ICommand<Task>;