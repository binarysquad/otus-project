using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record ConfirmUserCommand(string UserId, string Code) : ICommand<Task>;