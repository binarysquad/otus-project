using BlogPlus.Authorization.Application.Models;
using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public sealed record LoginUserCommand(string Email, string Password, bool RememberMe) : ICommand<LoginOutputDto>;