using BlogPlus.Authorization.Application.Models;
using Mediator;

namespace BlogPlus.Authorization.Application.Commands;

public record RegisterUserCommand(string Email, string Password) : ICommand<RegisterOutputDto>;