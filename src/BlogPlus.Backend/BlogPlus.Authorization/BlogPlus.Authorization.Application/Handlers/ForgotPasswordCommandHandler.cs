using System.Text;
using System.Text.Encodings.Web;
using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Services;
using BlogPlus.Authorization.Domain.Models;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class ForgotPasswordCommandHandler : ICommandHandler<ForgotPasswordCommand, Task>
{
    private readonly UserManager<User> _userManager;
    private readonly IEmailSender _emailSender;
    private readonly IUrlHelper _urlHelper;

    public ForgotPasswordCommandHandler(UserManager<User> userManager, IUrlHelper urlHelper, IEmailSender emailSender)
    {
        _userManager = userManager;
        _urlHelper = urlHelper;
        _emailSender = emailSender;
    }

    public async ValueTask<Task> Handle(ForgotPasswordCommand command, CancellationToken cancellationToken)
    {
        var user = await _userManager.Users
            .AsNoTracking()
            .SingleOrDefaultAsync(u => u.Email == command.Email, cancellationToken: cancellationToken);
        
        var code = await _userManager.GeneratePasswordResetTokenAsync(user);
        var callbackUrl = _urlHelper.Action(
            "ResetPassword",
            "Authorization",
            new { userId = user.Id, code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code)) },
            protocol: _urlHelper.ActionContext.HttpContext.Request.Scheme);
        
        await _emailSender.SendEmailAsync(
            command.Email, 
            "Cброс пароля",
            $"Для сброса пароля пройдите по ссылке: <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
        
        return Task.CompletedTask;
    }
}