using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class ConfirmUserCommandHandler : ICommandHandler<ConfirmUserCommand, Task>
{
    private readonly UserManager<User> _userManager;

    public ConfirmUserCommandHandler(UserManager<User> userManager)
    {
        _userManager = userManager;
    }

    public async ValueTask<Task> Handle(ConfirmUserCommand command, CancellationToken cancellationToken)
    {
        var rows = await _userManager.Users
            .Where(user => user.Id == command.UserId)
            .ExecuteUpdateAsync(s => 
                s.SetProperty(u => u.EmailConfirmed, true), cancellationToken: cancellationToken);
        
        if (rows == 0)
            throw new IdentityException(AuthorizationResources.UserCannotConfirmed);
        
        return Task.CompletedTask;
    }
}