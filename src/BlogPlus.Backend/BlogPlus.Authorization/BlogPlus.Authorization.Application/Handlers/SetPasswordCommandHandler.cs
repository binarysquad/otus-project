using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class SetPasswordCommandHandler : ICommandHandler<SetPasswordCommand, Task>
{
    private readonly UserManager<User> _userManager;

    public SetPasswordCommandHandler(UserManager<User> userManager)
    {
        _userManager = userManager;
    }

    public async ValueTask<Task> Handle(SetPasswordCommand command, CancellationToken cancellationToken)
    {
        var user = await _userManager.Users
            .AsNoTracking()
            .SingleOrDefaultAsync(u => u.Id == command.UserId, cancellationToken: cancellationToken);
        
        var passwordHash = _userManager.PasswordHasher.HashPassword(user, command.Password);
        var rows = await _userManager.Users
            .ExecuteUpdateAsync(s => s
                    .SetProperty(u => u.PasswordHash, passwordHash)
                    .SetProperty(u => u.SecurityStamp, _userManager.GenerateNewAuthenticatorKey()),
            cancellationToken: cancellationToken);
        
        if (rows == 0)
            throw new IdentityException(AuthorizationResources.PasswordHasNotBeenUpdated);

        return Task.CompletedTask;
    }
}