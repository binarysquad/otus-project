using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Common.Caching.CacheStore;
using Mediator;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class LogoutUserCommandHandler : ICommandHandler<LogoutUserCommand, Task>
{
    private readonly IDeactivatedTokenStore _deactivatedTokenStore;

    public LogoutUserCommandHandler(IDeactivatedTokenStore deactivatedTokenStore)
    {
        _deactivatedTokenStore = deactivatedTokenStore;
    }

    public async ValueTask<Task> Handle(LogoutUserCommand command, CancellationToken cancellationToken)
    {
        await _deactivatedTokenStore.DeactivateAsync(command.AccessToken);
        
        return Task.CompletedTask;
    }
}