﻿using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Authorization.Application.Services;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Common.AspNetCore.Exceptions;
using Mapster;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class LoginUserCommandHandler : ICommandHandler<LoginUserCommand, LoginOutputDto>
{
    private readonly UserManager<User> _userManager;
    private readonly IJwtManager _jwtManager;

    public LoginUserCommandHandler(UserManager<User> userManager, IJwtManager jwtManager)
    {
        _userManager = userManager;
        _jwtManager = jwtManager;
    }

    public async ValueTask<LoginOutputDto> Handle(LoginUserCommand command, CancellationToken cancellationToken)
    {
        var user = await _userManager.FindByEmailAsync(command.Email);
        
        var isAllowed = await _userManager.CheckPasswordAsync(user, command.Password);
        if (!isAllowed)
        {
            await _userManager.AccessFailedAsync(user);
            throw new UserLockedException(AuthorizationResources.InvalidPassword);
        }

        if (user.AccessFailedCount > 0)
        {
            await _userManager.Users
                .ExecuteUpdateAsync(s => 
                    s.SetProperty(u => u.AccessFailedCount, 0), cancellationToken: cancellationToken);
        }
        
        return _jwtManager.GenerateJwtToken(user).Adapt<LoginOutputDto>();
    }
}