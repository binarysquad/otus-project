﻿using System.Net;
using System.Text;
using System.Text.Encodings.Web;
using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Authorization.Application.Services;
using BlogPlus.Common.AspNetCore.Exceptions;
using Mediator;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

namespace BlogPlus.Authorization.Application.Handlers;

public sealed class RegisterUserCommandHandler : ICommandHandler<RegisterUserCommand, RegisterOutputDto>
{
    private readonly UserManager<User> _userManager;
    private readonly IEmailSender _emailSender;
    private readonly IUrlHelper _urlHelper;
    
    public RegisterUserCommandHandler(UserManager<User> userManager, IEmailSender emailSender, IUrlHelper urlHelper)
    {
        _userManager = userManager;
        _emailSender = emailSender;
        _urlHelper = urlHelper;
    }

    public async ValueTask<RegisterOutputDto> Handle(RegisterUserCommand command, CancellationToken cancellationToken)
    {
        var user = new User
        {
            Email = command.Email,
            UserName = command.Email
        };
        
        var result = await _userManager.CreateAsync(user, command.Password);
        if (!result.Succeeded)
            throw new IdentityException(AuthorizationResources.UserCannotCreated);
        
        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
        var callbackUrl = _urlHelper.Action(
            "Confirm",
            "Authorization",
            new { userId = user.Id, code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code)) },
            protocol: _urlHelper.ActionContext.HttpContext.Request.Scheme);
        
        await _emailSender.SendEmailAsync(
            command.Email, 
            "Confirm your email",
            $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

        return new RegisterOutputDto(HttpStatusCode.Created);
    }
}