using System.Diagnostics;
using BlogPlus.Authorization.Api.Configuration;
using BlogPlus.Common.AspNetCore.Middleware;

Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
Trace.AutoFlush = true;
Trace.Indent();

try
{
    Trace.WriteLine("Starting host...");
    
    TaskScheduler.UnobservedTaskException += (_, e) => Trace.WriteLine(e.Exception, "Unhandled exception was thrown");

    var app = AppHostBuilder.Build(args);
    
    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI();
    }
    else
    {
        app.UseHsts();
    }
    
    app.UseCookiePolicy();
    app.UseMiddleware<ExceptionMiddleware>();
    app.UseRouting();
    
    app.UseAuthentication();
    app.UseAuthorization();
    
    app.MapControllers();

    await app.RunAsync();
    return 0;
}
catch (Exception e)
{
    Trace.WriteLine(e, "An unhandled exception occured starting application.");
    return 1;
}