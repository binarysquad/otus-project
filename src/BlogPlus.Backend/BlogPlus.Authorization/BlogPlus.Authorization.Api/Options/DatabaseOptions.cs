using System.ComponentModel.DataAnnotations;

namespace BlogPlus.Authorization.Api.Options;

internal sealed record DatabaseOptions
{
    [Required]
    public string ConnectionString { get; init; }
    
    [Required]
    [Range(0, 1000)]
    public int CommandTimeout { get; init; }
}