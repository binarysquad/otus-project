﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class JsonExtension
{
    internal static IServiceCollection AddJsonDefaultConfiguration(this IServiceCollection services)
    {
        services.AddSingleton(new JsonSerializerOptions
        {
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });
        
        return services;
    }
}