using System.Reflection;
using BlogPlus.Common.AspNetCore.Configuration;
using BlogPlus.Common.Authorization.Configuration;
using BlogPlus.Common.Caching.Configuration;

namespace BlogPlus.Authorization.Api.Configuration;

public static class AppHostBuilder
{
    public static WebApplication Build(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Configuration.AddUserSecrets(Assembly.GetExecutingAssembly(), true);

        builder.Services.AddConfiguredAuthorization();
        builder.Services.AddRedisCache();
        builder.Services.AddWebServices();
        builder.Services.AddNpgsql();
        builder.Services.AddMediatorConfiguration();
        builder.Services.AddValidationConfiguration();
        builder.Services.AddAspNetIdentity();
        builder.Services.AddSwaggerSupport();
        builder.Services.AddJsonDefaultConfiguration();
        builder.Services.AddEmailClient();
        builder.Services.AddServices();
        
        return builder.Build();
    }
}