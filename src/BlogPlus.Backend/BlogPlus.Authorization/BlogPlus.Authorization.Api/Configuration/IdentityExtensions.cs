using BlogPlus.Authorization.Domain.Models;
using BlogPlus.Authorization.Persistence;
using Microsoft.AspNetCore.Identity;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class IdentityExtensions
{
    internal static IServiceCollection AddAspNetIdentity(this IServiceCollection services)
    {
        services
            .AddIdentity<User, Role>()
            .AddEntityFrameworkStores<IdentityContext>()
            .AddDefaultTokenProviders();
        
        // Настройка Identity
        services.Configure<IdentityOptions>(options =>
        {
            // Password settings.
            options.Password.RequireDigit = true;
            options.Password.RequireLowercase = true;
            options.Password.RequireUppercase = true;
            options.Password.RequireNonAlphanumeric = true;
            options.Password.RequiredLength = 8;
            options.Password.RequiredUniqueChars = 1;

            // Lockout settings.
            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
            options.Lockout.MaxFailedAccessAttempts = 5;
            options.Lockout.AllowedForNewUsers = true;

            // SignIn settings.
            options.SignIn.RequireConfirmedAccount = true;
            options.SignIn.RequireConfirmedEmail = true;

            // User settings.
            options.User.AllowedUserNameCharacters = null;
            options.User.RequireUniqueEmail = true;
        });

        return services;
    }
}