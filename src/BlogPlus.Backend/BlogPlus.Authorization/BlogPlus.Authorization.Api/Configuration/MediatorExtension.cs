﻿using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Models;
using BlogPlus.Authorization.Application.Validators.Confirm;
using BlogPlus.Authorization.Application.Validators.Forgot;
using BlogPlus.Authorization.Application.Validators.Login;
using BlogPlus.Authorization.Application.Validators.Register;
using BlogPlus.Authorization.Application.Validators.Reset;
using BlogPlus.Authorization.Application.Validators.SetPassword;
using Mediator;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class MediatorExtension
{
    internal static IServiceCollection AddMediatorConfiguration(this IServiceCollection services)
    {
        services.AddMediator(options =>
        {
            options.ServiceLifetime = ServiceLifetime.Scoped;
        });
        
        services.AddScoped<IPipelineBehavior<RegisterUserCommand, RegisterOutputDto>, RegisterCommandValidator>();
        services.AddScoped<IPipelineBehavior<ConfirmUserCommand, Task>, ConfirmCommandValidator>();
        services.AddScoped<IPipelineBehavior<LoginUserCommand, LoginOutputDto>, LoginCommandValidator>();
        services.AddScoped<IPipelineBehavior<ForgotPasswordCommand, Task>, ForgotCommandValidator>();
        services.AddScoped<IPipelineBehavior<ResetPasswordCommand, Task>, ResetCommandValidator>();
        services.AddScoped<IPipelineBehavior<SetPasswordCommand, Task>, SetPasswordCommandValidator>();
        
        return services;
    }
}