﻿using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Application.Validators.Confirm;
using BlogPlus.Authorization.Application.Validators.Forgot;
using BlogPlus.Authorization.Application.Validators.Login;
using BlogPlus.Authorization.Application.Validators.Register;
using BlogPlus.Authorization.Application.Validators.Reset;
using BlogPlus.Authorization.Application.Validators.SetPassword;
using FluentValidation;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class ValidationExtension
{
    internal static IServiceCollection AddValidationConfiguration(this IServiceCollection services)
    {
        services.AddScoped<IValidator<RegisterUserCommand>, RegisterModelValidator>();
        services.AddScoped<IValidator<ConfirmUserCommand>, ConfirmModelValidator>();
        services.AddScoped<IValidator<LoginUserCommand>, LoginModelValidator>();
        services.AddScoped<IValidator<ForgotPasswordCommand>, ForgotPasswordModelValidator>();
        services.AddScoped<IValidator<ResetPasswordCommand>, ResetPasswordModelValidator>();
        services.AddScoped<IValidator<SetPasswordCommand>, SetPasswordModelValidator>();

        return services;
    }
}