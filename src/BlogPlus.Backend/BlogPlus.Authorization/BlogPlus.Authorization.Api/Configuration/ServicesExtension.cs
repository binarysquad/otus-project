﻿using BlogPlus.Authorization.Application.Services;
using BlogPlus.Authorization.Application.Services.Implementation;
using BlogPlus.Authorization.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class ServicesExtension
{
    internal static IServiceCollection AddServices(this IServiceCollection services)
    {
        services.AddHostedService<InitializeDatabaseService>();
        
        services.AddScoped<IEmailSender, EmailSender>();
        services.AddScoped<IJwtManager, JwtManager>();
        
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        
        services.AddScoped<IUrlHelper>(x => {
            var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
            var factory = x.GetRequiredService<IUrlHelperFactory>();
            
            return factory.GetUrlHelper(actionContext);
        });
        
        return services;
    }
}