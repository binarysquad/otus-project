namespace BlogPlus.Authorization.Api.Configuration;

internal static class WebExtensions
{
    internal static IServiceCollection AddWebServices(this IServiceCollection services)
    {
        services.AddRouting(options =>
        {
            options.LowercaseUrls = true;
            options.LowercaseQueryStrings = false;
        });
        
        services.AddControllers();

        return services;
    }
}