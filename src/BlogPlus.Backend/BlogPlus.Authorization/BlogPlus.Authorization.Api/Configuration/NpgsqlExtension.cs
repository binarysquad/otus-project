using BlogPlus.Authorization.Persistence;
using BlogPlus.Authorization.Api.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class NpgsqlExtension
{
    internal static IServiceCollection AddNpgsql(this IServiceCollection services)
    {
        services
            .AddOptions<DatabaseOptions>()
            .BindConfiguration(nameof(DatabaseOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();
        
        services.AddDbContext<IdentityContext>((serviceProvider, optionsBuilder) =>
        {
            var serviceOptions = serviceProvider.GetRequiredService<IOptions<DatabaseOptions>>().Value;
            
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            optionsBuilder.UseNpgsql(
                serviceOptions.ConnectionString,
                options =>
                {
                    options.CommandTimeout(serviceOptions.CommandTimeout);
                });
        });
        
        return services;
    }
}