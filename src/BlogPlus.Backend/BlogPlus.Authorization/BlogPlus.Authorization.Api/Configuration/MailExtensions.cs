﻿using BlogPlus.Authorization.Application.Options;

namespace BlogPlus.Authorization.Api.Configuration;

internal static class MailExtensions
{
    internal static IServiceCollection AddEmailClient(this IServiceCollection services)
    {
        services
            .AddOptions<MessageSenderOptions>()
            .BindConfiguration(nameof(MessageSenderOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();
        
        return services;
    }
}