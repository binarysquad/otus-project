﻿FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["BlogPlus.Authorization.Api/BlogPlus.Authorization.Api.csproj", "BlogPlus.Authorization.Api/"]
COPY ["BlogPlus.Authorization.Application/BlogPlus.Authorization.Application.csproj", "BlogPlus.Authorization.Application/"]
COPY ["BlogPlus.Authorization.Domain/BlogPlus.Authorization.Domain.csproj", "BlogPlus.Authorization.Domain/"]
COPY ["BlogPlus.Common.AspNetCore/BlogPlus.Common.AspNetCore.csproj", "BlogPlus.Common.AspNetCore/"]
COPY ["BlogPlus.Common.Caching/BlogPlus.Common.Caching.csproj", "BlogPlus.Common.Caching/"]
COPY ["BlogPlus.Common.Authorization/BlogPlus.Common.Authorization.csproj", "BlogPlus.Common.Authorization/"]
COPY ["BlogPlus.Authorization.Persistence/BlogPlus.Authorization.Persistence.csproj", "BlogPlus.Authorization.Persistence/"]
RUN dotnet restore "BlogPlus.Authorization.Api/BlogPlus.Authorization.Api.csproj"
COPY . .
WORKDIR "/src/BlogPlus.Authorization.Api"
RUN dotnet build "BlogPlus.Authorization.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "BlogPlus.Authorization.Api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BlogPlus.Authorization.Api.dll"]
