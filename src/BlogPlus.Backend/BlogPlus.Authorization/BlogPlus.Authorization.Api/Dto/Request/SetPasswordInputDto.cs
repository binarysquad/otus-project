﻿namespace BlogPlus.Authorization.Api.Dto.Request;

public sealed record SetPasswordInputDto(string Password, string ConfirmPassword);