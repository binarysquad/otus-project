using Mediator;

namespace BlogPlus.Authorization.Api.Dto.Request;

public sealed record ResetPasswordInputDto(string Email, string Password, string ConfirmPassword, string Code) : ICommand<Task>;