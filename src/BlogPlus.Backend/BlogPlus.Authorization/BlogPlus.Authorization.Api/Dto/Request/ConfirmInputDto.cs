namespace BlogPlus.Authorization.Api.Dto.Request;

public sealed record ConfirmInputDto(string UserId, string Code);