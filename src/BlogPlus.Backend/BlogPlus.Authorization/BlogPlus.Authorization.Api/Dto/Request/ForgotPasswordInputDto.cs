using Mediator;

namespace BlogPlus.Authorization.Api.Dto.Request;

public sealed record ForgotPasswordInputDto(string Email) : ICommand<Task>;