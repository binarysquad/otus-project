namespace BlogPlus.Authorization.Api.Dto.Request;

public record LoginInputDto(string Email, string Password, bool RememberMe);