﻿namespace BlogPlus.Authorization.Api.Dto.Request;

public sealed record RegisterInputDto(string Email, string Password);