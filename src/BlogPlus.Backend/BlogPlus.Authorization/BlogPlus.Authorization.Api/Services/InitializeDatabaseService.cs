﻿using BlogPlus.Authorization.Persistence;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.Authorization.Api.Services;

internal sealed class InitializeDatabaseService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;

    public InitializeDatabaseService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceProvider.CreateScope();
        
        var dbContext = scope.ServiceProvider.GetRequiredService<IdentityContext>();
        await dbContext.Database.MigrateAsync(cancellationToken: cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}