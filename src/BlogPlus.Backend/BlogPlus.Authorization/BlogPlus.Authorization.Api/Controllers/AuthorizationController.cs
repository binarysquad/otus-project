using System.Net;
using System.Security.Claims;
using BlogPlus.Authorization.Application.Commands;
using BlogPlus.Authorization.Api.Dto.Request;
using BlogPlus.Common.AspNetCore.Extensions;
using Mapster;
using Mediator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace BlogPlus.Authorization.Api.Controllers;

/// <summary>
/// Авторизация.
/// </summary>
[ApiController]
[ApiVersion("1.0")]
[Produces("application/json")]
[Route($"api/v{{v:apiVersion}}/[controller]")]
public sealed class AuthorizationController : ControllerBase
{
    private readonly IMediator _mediator;

    public AuthorizationController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] RegisterInputDto input)
    {
        var response = await _mediator.Send(input.Adapt<RegisterUserCommand>(), HttpContext.RequestAborted);
        
        return response.StatusCode == HttpStatusCode.Created 
            ? CreatedAtAction(nameof(Login), null)
            : StatusCode((int) response.StatusCode);
    }
    
    [HttpGet("confirm")]
    public async Task<IActionResult> Confirm([FromQuery] ConfirmInputDto input)
    {
        await _mediator.Send(input.Adapt<ConfirmUserCommand>(), HttpContext.RequestAborted);

        return Ok("Email подтверждён");
    }
    
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginInputDto input)
    {
        var response = await _mediator.Send(input.Adapt<LoginUserCommand>(), HttpContext.RequestAborted);

        return Ok(response);
    }
    
    [HttpPost("logout")]
    [Authorize]
    public async Task<IActionResult> Logout()
    {
        await _mediator.Send(new LogoutUserCommand(HttpContext.GetAccessToken()), HttpContext.RequestAborted);

        return NoContent();
    }
    
    [HttpPost("forgot")]
    public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordInputDto input)
    {
        await _mediator.Send(input.Adapt<ForgotPasswordCommand>(), HttpContext.RequestAborted);

        return NoContent();
    }
    
    [HttpPost("reset")]
    public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordInputDto input)
    {
        await _mediator.Send(input.Adapt<ResetPasswordCommand>(), HttpContext.RequestAborted);

        return Ok("Пароль успешно сброшен.");
    }
    
    [HttpPut("password")]
    [Authorize]
    public async Task<IActionResult> SetPassword([FromBody] SetPasswordInputDto input)
    {
        await _mediator.Send(new SetPasswordCommand(input.Password, input.ConfirmPassword, User.FindFirstValue(JwtRegisteredClaimNames.Sub)), HttpContext.RequestAborted);
        
        return Ok();
    }
}