﻿namespace BlogPlus.ContentHub.Domain.Entities;

public class ArticleTopic 
{
    /// <summary>
    /// Идентификатор статьи, к которой привязан топик
    /// </summary>
    public int ArticleId { get; set; }

    /// <summary>
    /// Статья, к которой привязан топик
    /// </summary>
    public Article Article { get; set; }

    /// <summary>
    /// Идентификатор топика
    /// </summary>
    public int TopicId { get; set; }

    /// <summary>
    /// Топик
    /// </summary>
    public Topic Topic { get; set; }
}