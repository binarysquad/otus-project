﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность компании
/// </summary>
public class Company : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Идентификатор пользователя, являющегося владельцем компании
    /// </summary>
    public Guid OwnerId { get; set; }
    
    /// <summary>
    /// Владелец компании
    /// </summary>
    public UserProfile Owner { get; set; }

    /// <summary>
    /// Название компании
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Описание компании
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Дата создания
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    /// Дата последнего изменения
    /// </summary>
    public DateTime ModifiedDate { get; set; }
    
    /// <summary>
    /// Статьи компании
    /// </summary>
    public ICollection<Article> Articles { get; set; }
    
    /// <summary>
    /// Участники компании
    /// </summary>
    public ICollection<CompanyUser> CompanyUsers { get; set; }
    
    /// <summary>
    /// Пользователи
    /// </summary>
    public ICollection<UserProfile> Users { get; set; }
}
