﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность комментария к статье
/// </summary>
public class Comment : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Идентификатор статьи, к которой написан комментарий
    /// </summary>
    public int ArticleId { get; set; }

    /// <summary>
    /// Статья, к которой написан комментарий
    /// </summary>
    public Article Article { get; set; }

    /// <summary>
    /// Идентификатор пользователя, написавшего комментарий
    /// </summary>
    public Guid UserId { get; set; }

    /// <summary>
    /// Пользователь, написавший комментарий
    /// </summary>
    public UserProfile User { get; set; }

    /// <summary>
    /// Содержимое комментария
    /// </summary>
    public string Content { get; set; }

    /// <summary>
    /// Дата создания
    /// </summary>
    public DateTime CreatedDate { get; set; }
}
