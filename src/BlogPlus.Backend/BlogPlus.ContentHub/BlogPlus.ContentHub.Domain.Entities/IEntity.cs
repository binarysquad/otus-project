﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Интерфейс сущности из БД с идентификатором
/// </summary>
/// <typeparam name="TId">Тип идентификатора</typeparam>
public interface IEntity<TId>
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    public TId Id { get; set; }
}