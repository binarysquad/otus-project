﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность статьи
/// </summary>
public class Article : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Название статьи
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// Описание статьи
    /// </summary>
    public string Description { get; set; }
    
    /// <summary>
    /// Тело статьи
    /// </summary>
    public string Body { get; set; }

    /// <summary>
    /// Идентификатор автора статьи
    /// </summary>
    public Guid AuthorId { get; set; }

    /// <summary>
    /// Автор статьи
    /// </summary>
    public UserProfile Author { get; set; }
    
    /// <summary>
    /// Идентификатор компании, опубликовавшей статью
    /// Если null - статья опубликована самим автором
    /// </summary>
    public int? CompanyId { get; set; }

    /// <summary>
    /// Компания, опубликовавшая статью
    /// </summary>
    public Company? Company { get; set; }
    
    /// <summary>
    /// Идентификатор статуса статьи
    /// </summary>
    public int StatusId { get; set; }
    
    /// <summary>
    /// Статус статьи
    /// </summary>
    public ArticleStatus Status { get; set; }
    
    /// <summary>
    /// Дата публикации статьи
    /// </summary>
    public DateTime PublicationDate { get; set; }

    /// <summary>
    /// Дата последнего изменения статьи
    /// </summary>
    public DateTime ModifiedDate { get; set; }

    /// <summary>
    /// Дата создания статьи
    /// </summary>
    public DateTime CreatedDate { get; set; }
    
    /// <summary>
    /// Комментарии к статье
    /// </summary>
    public ICollection<Comment> Comments { get; set; }
    
    /// <summary>
    /// Топики статьи
    /// </summary>
    public ICollection<Topic> Topics { get; set; }
    
    /// <summary>
    /// Связывающая статью и топик сущность
    /// </summary>
    public ICollection<ArticleTopic> ArticleTopics { get; set; }
}