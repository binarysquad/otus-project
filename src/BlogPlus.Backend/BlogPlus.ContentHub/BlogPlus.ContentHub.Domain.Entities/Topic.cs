﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность топика статьи
/// </summary>
public class Topic : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Название
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Статьи, которые используют данный топик
    /// </summary>
    public ICollection<Article> Articles { get; set; }
    
    /// <summary>
    /// Связывающая статью и топик сущность
    /// </summary>
    public ICollection<ArticleTopic> ArticleTopics { get; set; }
}