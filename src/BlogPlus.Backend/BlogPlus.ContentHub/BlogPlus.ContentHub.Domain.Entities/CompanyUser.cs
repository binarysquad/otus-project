﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность участника компании
/// </summary>
public class CompanyUser : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Идентификатор компании, к которой привязан участник
    /// </summary>
    public int CompanyId { get; set; }

    /// <summary>
    /// Компания, к которой привязан участник
    /// </summary>
    public Company Company { get; set; }

    /// <summary>
    /// Идентификатор пользователя, являющегося участником компании
    /// </summary>
    public Guid UserId { get; set; }

    /// <summary>
    /// Пользователь, являющийся участником компании
    /// </summary>
    public UserProfile User { get; set; }
    
    /// <summary>
    /// Является ли участник модератором
    /// </summary>
    public bool IsModerator { get; set; }

    /// <summary>
    /// Дата создания
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    /// Дата последнего изменения
    /// </summary>
    public DateTime ModifiedDate { get; set; }
}
