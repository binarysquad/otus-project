﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность статуса статьи
/// </summary>
public class ArticleStatus : IEntity<int>
{
    /// <inheritdoc />
    public int Id { get; set; }

    /// <summary>
    /// Название статуса
    /// </summary>
    public string Name { get; set; }   
    
    /// <summary>
    /// Навигационное свойство по статьям, имеющим данный статус
    /// </summary>
    public ICollection<Article> Articles { get; set; }
}
