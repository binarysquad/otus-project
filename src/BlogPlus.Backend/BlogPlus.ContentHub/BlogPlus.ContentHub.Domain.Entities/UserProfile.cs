﻿namespace BlogPlus.ContentHub.Domain.Entities;

/// <summary>
/// Сущность профиля пользователя
/// </summary>
public class UserProfile : IEntity<Guid>
{
    /// <inheritdoc />
    public Guid Id { get; set; }

    /// <summary>
    /// Почта
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// Имя
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// Фамилия
    /// </summary>
    public string LastName { get; set; }

    /// <summary>
    /// Дата создания
    /// </summary>
    public DateTime CreatedDate { get; set; }

    /// <summary>
    /// Дата последнего изменения
    /// </summary>
    public DateTime ModifiedDate { get; set; }

    /// <summary>
    /// Является ли аккаунт активным
    /// </summary>
    public bool IsActive { get; set; }

    /// <summary>
    /// Статьи пользователя
    /// </summary>
    public ICollection<Article> Articles { get; set; }
    
    /// <summary>
    /// Комментарии, написанные пользователем
    /// </summary>
    public ICollection<Comment> Comments { get; set; }
    
    /// <summary>
    /// Компании пользователя, где он является владельцем
    /// </summary>
    public ICollection<Company> OwnedCompanies { get; set; }
    
    /// <summary>
    /// Компании пользователя, где он является участником
    /// </summary>
    public ICollection<Company> Companies { get; set; }
    
    /// <summary>
    /// Связывающая сущность, представляющая участника компании
    /// </summary>
    public ICollection<CompanyUser> CompanyUsers { get; set; }
}
