﻿using System.Collections.ObjectModel;
using BlogPlus.ContentHub.Application.Contracts.Dtos;
using BlogPlus.ContentHub.Application.Contracts.Filters;
using BlogPlus.ContentHub.Application.Repositories.Abstractions;
using BlogPlus.ContentHub.Application.Services.Abstractions;

namespace BlogPlus.ContentHub.Application.Services.Implementation;

/// <inheritdoc />
public class ArticleService : IArticleService
{
    private readonly IArticleRepository _articleRepository;
    
    /// <inheritdoc />
    public async Task<ShortArticleDto> GetShortArticleByIdAsync(int id)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public async Task<FullArticleDto> GetFullArticleByIdAsync(int id)
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc />
    public async Task<Collection<ShortArticleDto>> GetShortArticlesByFilterAsync(ArticleFilter filter)
    {
        throw new NotImplementedException();
    }
}