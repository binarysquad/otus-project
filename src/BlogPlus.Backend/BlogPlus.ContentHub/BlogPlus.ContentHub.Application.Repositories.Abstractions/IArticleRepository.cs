﻿using System.Collections.ObjectModel;
using BlogPlus.ContentHub.Application.Contracts.Filters;
using BlogPlus.ContentHub.Domain.Entities;

namespace BlogPlus.ContentHub.Application.Repositories.Abstractions;

/// <summary>
/// Репозиторий для работы со статьями
/// </summary>
public interface IArticleRepository : IBaseRepository<Article, int>
{
    /// <summary>
    /// Получить список статей, используя фильтр
    /// </summary>
    /// <param name="filter">Фильтр статей</param>
    /// <returns>Список статей</returns>
    List<Article> GetArticlesByFilter(ArticleFilter filter);
    
    /// <summary>
    /// Получить список статей, используя фильтр
    /// </summary>
    /// <param name="filter">Фильтр статей</param>
    /// <returns>Список статей</returns>
    Task<Collection<Article>> GetArticlesByFilterAsync(ArticleFilter filter);
}