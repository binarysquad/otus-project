﻿using BlogPlus.ContentHub.Domain.Entities;

namespace BlogPlus.ContentHub.Application.Repositories.Abstractions;

/// <summary>
/// Базовый обобщенный репозиторий
/// </summary>
/// <typeparam name="TEntity">Тип сущности</typeparam>
/// <typeparam name="TPrimaryKey">Тип первичного ключа</typeparam>
public interface IBaseRepository<TEntity, TPrimaryKey> where TEntity : class, IEntity<TPrimaryKey>
{
    /// <summary>
    /// Получить сущность по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Сущность</returns>
    TEntity GetById(TPrimaryKey id);
    
    /// <summary>
    /// Получить сущность по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Сущность</returns>
    Task<TEntity> GetByIdAsync(TPrimaryKey id);
}