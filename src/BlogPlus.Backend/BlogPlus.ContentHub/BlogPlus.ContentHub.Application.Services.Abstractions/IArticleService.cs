﻿using System.Collections.ObjectModel;
using BlogPlus.ContentHub.Application.Contracts.Dtos;
using BlogPlus.ContentHub.Application.Contracts.Filters;

namespace BlogPlus.ContentHub.Application.Services.Abstractions;

/// <summary>
/// Сервис для работы со статьями
/// </summary>
public interface IArticleService
{
    /// <summary>
    /// Получить краткую статью по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор статьт</param>
    /// <returns>Краткая модель статьи</returns>
    Task<ShortArticleDto> GetShortArticleByIdAsync(int id);
    
    /// <summary>
    /// Получить полную информацию о статью по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор статье</param>
    /// <returns>Модель с полной информацией о статье</returns>
    Task<FullArticleDto> GetFullArticleByIdAsync(int id);
    
    /// <summary>
    /// Получить полную информацию о статью по идентификатору
    /// </summary>
    /// <param name="filter">Фильтр</param>
    /// <returns>Модель с полной информацией о статье</returns>
    Task<Collection<ShortArticleDto>> GetShortArticlesByFilterAsync(ArticleFilter filter);
}