﻿using System.Collections.ObjectModel;
using BlogPlus.ContentHub.Application.Contracts.Filters;
using BlogPlus.ContentHub.Application.Repositories.Abstractions;
using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.ContentHub.Infrastructure.Repositories.Implementation;

/// <summary>
/// Репозиторий для работы со статьями
/// </summary>
public class ArticleRepository : BaseRepository<Article, int>, IArticleRepository
{
    public ArticleRepository(DbContext context) : base(context)
    {
    }

    public List<Article> GetArticlesByFilter(ArticleFilter filter)
    {
        throw new NotImplementedException();
    }

    public async Task<Collection<Article>> GetArticlesByFilterAsync(ArticleFilter filter)
    {
        throw new NotImplementedException();
    }
}