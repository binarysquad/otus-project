﻿using BlogPlus.ContentHub.Application.Repositories.Abstractions;
using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.ContentHub.Infrastructure.Repositories.Implementation;

/// <inheritdoc />
public abstract class BaseRepository<TEntity, TPrimaryKey> : IBaseRepository<TEntity, TPrimaryKey> 
    where TEntity : class, IEntity<TPrimaryKey>
{
    protected readonly DbContext Context;
    private readonly DbSet<TEntity> _entities;

    protected BaseRepository(DbContext context)
    {
        Context = context;
        _entities = Context.Set<TEntity>();
    }
    
    /// <inheritdoc />
    public TEntity GetById(TPrimaryKey id)
    {
        return _entities.Find(id);
    }

    /// <inheritdoc />
    public async Task<TEntity> GetByIdAsync(TPrimaryKey id)
    {
        return await _entities.FindAsync(id);
    }
    
    /// <summary>
    /// Сохранить изменения
    /// </summary>
    public virtual void SaveChanges()
    {
        Context.SaveChanges();
    }

    /// <summary>
    /// Сохранить изменения
    /// </summary>
    public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await Context.SaveChangesAsync(cancellationToken);
    }
}