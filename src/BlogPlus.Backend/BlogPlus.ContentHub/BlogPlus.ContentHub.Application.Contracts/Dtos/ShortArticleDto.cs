﻿namespace BlogPlus.ContentHub.Application.Contracts.Dtos;

/// <summary>
/// Модель с краткой информацией о статье
/// </summary>
public class ShortArticleDto
{
    /// <summary>
    /// Идентификатор статьи
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название статьи
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// Описание статьи
    /// </summary>
    public string Description { get; set; }
    
    /// <summary>
    /// Идентификатор автора статьи
    /// </summary>
    public int AuthorId { get; set; }
}