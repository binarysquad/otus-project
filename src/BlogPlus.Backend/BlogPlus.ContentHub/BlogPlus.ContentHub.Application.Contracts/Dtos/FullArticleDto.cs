﻿namespace BlogPlus.ContentHub.Application.Contracts.Dtos;

/// <summary>
/// Модель с полной информацией о статье
/// </summary>
public class FullArticleDto
{
    /// <summary>
    /// Идентификатор статьи
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название статьи
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// Описание статьи
    /// </summary>
    public string Description { get; set; }
    
    /// <summary>
    /// Тело статьи
    /// </summary>
    public string Body { get; set; }

    /// <summary>
    /// Идентификатор автора статьи
    /// </summary>
    public int AuthorId { get; set; }
    
    /// <summary>
    /// Идентификатор компании, опубликовавшей статью
    /// Если null - статья опубликована самим автором
    /// </summary>
    public int? CompanyId { get; set; }
    
    /// <summary>
    /// Дата публикации статьи
    /// </summary>
    public DateTime PublicationDate { get; set; }
}