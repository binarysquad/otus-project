﻿namespace BlogPlus.ContentHub.Application.Contracts.Filters;

public class ArticleFilter
{
    /// <summary>
    /// Название статьи
    /// </summary>
    public string Title { get; set; }
    
    /// <summary>
    /// Идентификатор автора статьи
    /// </summary>
    public int? AuthorId { get; set; }
    
    /// <summary>
    /// Идентификатор компании, опубликовавшей статью
    /// </summary>
    public int? CompanyId { get; set; }
    
    /// <summary>
    /// Дата публикации статьи
    /// </summary>
    public DateTime PublicationDate { get; set; }
}