﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.Migrations
{
    /// <inheritdoc />
    public partial class AddTestData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "UsersProfiles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "UsersProfiles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "CompanyUsers",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "CompanyUsers",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Companies",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Companies",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Comments",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublicationDate",
                table: "Articles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Articles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Articles",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.InsertData(
                table: "ArticleStatuses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "draft" },
                    { 2, "company moderation" },
                    { 3, "automatic moderation" },
                    { 4, "administrator moderation" },
                    { 5, "published" },
                    { 6, "deleted" },
                    { 7, "rejected" }
                });

            migrationBuilder.InsertData(
                table: "UsersProfiles",
                columns: new[] { "Id", "CreatedDate", "Email", "FirstName", "IsActive", "LastName", "ModifiedDate" },
                values: new object[] { new Guid("3ea79e0f-0f92-4bf3-96da-1be767c44735"), new DateTime(2023, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "test_testov@mail.com", "Test", true, "Testov", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Articles",
                columns: new[] { "Id", "AuthorId", "Body", "CompanyId", "CreatedDate", "Description", "ModifiedDate", "PublicationDate", "StatusId", "Title" },
                values: new object[,]
                {
                    { 1, new Guid("3ea79e0f-0f92-4bf3-96da-1be767c44735"), "Итак, вы задались вопросом заполнения вашей бд тестовыми данными с помощью EntityFrameworkв час ночи. С чего следует начать, так это с чашки кофе, чтобы точно не уснуть раньше 5 утра и гарантированно проспать работу и быть овощем весь день.", null, new DateTime(2023, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Микрогайд про способы заполнения EF бд тестовыми данными в час ночи. Моя первая статья на блогплюсе, не судите строго плиз.", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2023, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Как заполнить EF бдшку тестовыми данными" },
                    { 2, new Guid("3ea79e0f-0f92-4bf3-96da-1be767c44735"), "Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1.", null, new DateTime(2023, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Описание статьи 1, небольшое и полезное описание", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2023, 8, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Тестовая статья 1" },
                    { 3, new Guid("3ea79e0f-0f92-4bf3-96da-1be767c44735"), "Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 .", null, new DateTime(2023, 8, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Самое лучшие описание для статьи 2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2023, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "Тестовая статья 2" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Articles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Articles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Articles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ArticleStatuses",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "UsersProfiles",
                keyColumn: "Id",
                keyValue: new Guid("3ea79e0f-0f92-4bf3-96da-1be767c44735"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "UsersProfiles",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "UsersProfiles",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "CompanyUsers",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "CompanyUsers",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Companies",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Companies",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Comments",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublicationDate",
                table: "Articles",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDate",
                table: "Articles",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Articles",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");
        }
    }
}
