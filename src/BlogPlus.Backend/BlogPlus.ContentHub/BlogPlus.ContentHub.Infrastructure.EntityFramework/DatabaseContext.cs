﻿using System.Reflection;
using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework;

/// <summary>
/// Контекст базы данных
/// </summary>
public class DatabaseContext : DbContext
{
    /// <summary>
    /// Пользователи
    /// </summary>
    public DbSet<UserProfile> UsersProfiles { get; set; }
    
    /// <summary>
    /// Статьи
    /// </summary>
    public DbSet<Article> Articles { get; set; }
    
    /// <summary>
    /// Комментарии
    /// </summary>
    public DbSet<Comment> Comments { get; set; }
    
    /// <summary>
    /// Статусы статьи
    /// </summary>
    public DbSet<ArticleStatus> ArticleStatuses { get; set; }
    
    /// <summary>
    /// Топики статьи
    /// </summary>
    public DbSet<Topic> Topics { get; set; }
    
    /// <summary>
    /// Связывающая таблица для статьи и топика
    /// </summary>
    public DbSet<ArticleTopic> ArticleTopics { get; set; }

    /// <summary>
    /// Компании
    /// </summary>
    public DbSet<Company> Companies { get; set; }
    
    /// <summary>
    /// Участники компании
    /// </summary>
    public DbSet<CompanyUser> CompanyUsers { get; set; }
    
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        modelBuilder.Entity<UserProfile>()
            .HasData(
                new UserProfile
                {
                    Id = Guid.Parse("3ea79e0f-0f92-4bf3-96da-1be767c44735"),
                    Email = "test_testov@mail.com",
                    FirstName = "Test",
                    LastName = "Testov",
                    CreatedDate = new DateTime(2023, 08, 21),
                    IsActive = true
                }
            );
        
        modelBuilder.Entity<Article>()
            .HasData(
                new Article
                {
                    Id = 1,
                    Title = "Как заполнить EF бдшку тестовыми данными",
                    Description = "Микрогайд про способы заполнения EF бд тестовыми данными в час ночи. Моя первая " +
                                  "статья на блогплюсе, не судите строго плиз.",
                    Body = "Итак, вы задались вопросом заполнения вашей бд тестовыми данными с помощью EntityFramework" +
                           "в час ночи. С чего следует начать, так это с чашки кофе, чтобы точно не уснуть раньше " +
                           "5 утра и гарантированно проспать работу и быть овощем весь день.",
                    AuthorId = Guid.Parse("3ea79e0f-0f92-4bf3-96da-1be767c44735"),
                    CompanyId = null,
                    StatusId = 5,
                    PublicationDate = new DateTime(2023, 08, 21),
                    CreatedDate = new DateTime(2023, 08, 21)
                }
            );
        
        modelBuilder.Entity<Article>()
            .HasData(
                new Article
                {
                    Id = 2,
                    Title = "Тестовая статья 1",
                    Description = "Описание статьи 1, небольшое и полезное описание",
                    Body = "Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 " +
                           "Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 " +
                           "Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 Текст статьи 1 " +
                           "Текст статьи 1 Текст статьи 1.",
                    AuthorId = Guid.Parse("3ea79e0f-0f92-4bf3-96da-1be767c44735"),
                    CompanyId = null,
                    StatusId = 5,
                    PublicationDate = new DateTime(2023, 08, 22),
                    CreatedDate = new DateTime(2023, 08, 21)
                }
            );
        
        modelBuilder.Entity<Article>()
            .HasData(
                new Article
                {
                    Id = 3,
                    Title = "Тестовая статья 2",
                    Description = "Самое лучшие описание для статьи 2",
                    Body = "Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 " +
                           "Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 " +
                           "Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 Текст статьи 2 .",
                    AuthorId = Guid.Parse("3ea79e0f-0f92-4bf3-96da-1be767c44735"),
                    CompanyId = null,
                    StatusId = 5,
                    PublicationDate = new DateTime(2023, 08, 25),
                    CreatedDate = new DateTime(2023, 08, 23)
                }
            );
        
        modelBuilder.Entity<ArticleStatus>()
            .HasData(
                new ArticleStatus
                {
                    Id = 1,
                    Name = "draft"
                },
                new ArticleStatus
                {
                    Id = 2,
                    Name = "company moderation"
                },
                new ArticleStatus
                {
                    Id = 3,
                    Name = "automatic moderation"
                },
                new ArticleStatus
                {
                    Id = 4,
                    Name = "administrator moderation"
                },
                new ArticleStatus
                {
                    Id = 5,
                    Name = "published"
                },
                new ArticleStatus
                {
                    Id = 6,
                    Name = "deleted"
                },
                new ArticleStatus
                {
                    Id = 7,
                    Name = "rejected"
                }
            );
    }
}