﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class ArticleConfiguration : IEntityTypeConfiguration<Article>
{
    public void Configure(EntityTypeBuilder<Article> builder)
    {
        builder.HasKey(e => e.Id);
        
        builder
            .Property(e => e.Title)
            .HasMaxLength(50)
            .IsRequired();
        
        builder
            .HasOne(e => e.Author)
            .WithMany(e => e.Articles)
            .HasForeignKey(e => e.AuthorId);
        
        builder
            .HasOne(e => e.Company)
            .WithMany(e => e.Articles)
            .HasForeignKey(e => e.CompanyId);
        
        builder
            .HasOne(e => e.Status)
            .WithMany(e => e.Articles)
            .HasForeignKey(e => e.StatusId);
        
        builder
            .HasMany(e => e.Comments)
            .WithOne(e => e.Article)
            .HasForeignKey(e => e.ArticleId);
        
        builder
            .HasMany(e => e.Topics)
            .WithMany(e => e.Articles)
            .UsingEntity<ArticleTopic>(
                l => l
                    .HasOne(e => e.Topic)
                    .WithMany(e => e.ArticleTopics)
                    .HasForeignKey(e => e.TopicId),
                r => r
                    .HasOne(e => e.Article)
                    .WithMany(e => e.ArticleTopics)
                    .HasForeignKey(e => e.ArticleId),
                b => b
                    .HasKey(e => new {e.ArticleId, e.TopicId})
            );
    }
}