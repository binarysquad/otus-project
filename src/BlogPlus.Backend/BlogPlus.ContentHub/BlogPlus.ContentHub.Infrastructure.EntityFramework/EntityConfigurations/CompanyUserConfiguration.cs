﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class CompanyUserConfiguration : IEntityTypeConfiguration<CompanyUser>
{
    public void Configure(EntityTypeBuilder<CompanyUser> builder)
    {
        builder.HasKey(e => e.Id);
        
        builder
            .Property(e => e.IsModerator)
            .IsRequired();

        builder
            .HasOne(e => e.Company)
            .WithMany(e => e.CompanyUsers)
            .HasForeignKey(e => e.CompanyId)
            .IsRequired();

        builder
            .HasOne(e => e.User)
            .WithMany(e => e.CompanyUsers)
            .HasForeignKey(e => e.UserId)
            .IsRequired();
    }
}