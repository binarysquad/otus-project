﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class ArticleStatustConfiguration : IEntityTypeConfiguration<ArticleStatus>
{
    public void Configure(EntityTypeBuilder<ArticleStatus> builder)
    {
        builder.HasKey(e => e.Id);
        
        builder
            .Property(e => e.Name)
            .HasMaxLength(30)
            .IsRequired();

        builder
            .HasMany(e => e.Articles)
            .WithOne(e => e.Status)
            .HasForeignKey(e => e.StatusId);
    }
}