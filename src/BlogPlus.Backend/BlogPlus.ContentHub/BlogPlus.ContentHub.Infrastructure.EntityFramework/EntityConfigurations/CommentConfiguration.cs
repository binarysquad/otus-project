﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class CommentConfiguration : IEntityTypeConfiguration<Comment>
{
    public void Configure(EntityTypeBuilder<Comment> builder)
    {
        builder.HasKey(e => e.Id);
        
        builder
            .Property(e => e.Content)
            .HasMaxLength(300)
            .IsRequired();

        builder
            .HasOne(e => e.Article)
            .WithMany(e => e.Comments)
            .HasForeignKey(e => e.ArticleId);
        
        builder
            .HasOne(e => e.User)
            .WithMany(e => e.Comments)
            .HasForeignKey(e => e.UserId);
    }
}