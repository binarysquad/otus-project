﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class CompanyConfiguration : IEntityTypeConfiguration<Company>
{
    public void Configure(EntityTypeBuilder<Company> builder)
    {
        builder.HasKey(e => e.Id);
        
        builder
            .Property(e => e.Name)
            .HasMaxLength(50)
            .IsRequired();

        builder
            .HasOne(e => e.Owner)
            .WithMany(e => e.OwnedCompanies)
            .HasForeignKey(e => e.OwnerId)
            .IsRequired();

        builder
            .HasMany(e => e.Articles)
            .WithOne(e => e.Company)
            .HasForeignKey(e => e.CompanyId);

        builder
            .HasMany(e => e.CompanyUsers)
            .WithOne(e => e.Company)
            .HasForeignKey(e => e.CompanyId);

        builder
            .HasMany(e => e.Users)
            .WithMany(e => e.Companies)
            .UsingEntity<CompanyUser>(
                r => r
                    .HasOne(e => e.User)
                    .WithMany(e => e.CompanyUsers)
                    .HasForeignKey(e => e.UserId),
                l => l
                    .HasOne(e => e.Company)
                    .WithMany(e => e.CompanyUsers)
                    .HasForeignKey(e => e.CompanyId),
                b => b
                    .HasKey(e => e.Id)
            );
    }
}