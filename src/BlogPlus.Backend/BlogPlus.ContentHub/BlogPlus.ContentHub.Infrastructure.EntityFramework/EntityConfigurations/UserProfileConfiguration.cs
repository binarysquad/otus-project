﻿using BlogPlus.ContentHub.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BlogPlus.ContentHub.Infrastructure.EntityFramework.EntityConfigurations;

internal sealed class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
{
    public void Configure(EntityTypeBuilder<UserProfile> builder)
    {
        builder.HasKey(e => e.Id);

        builder
            .Property(e => e.Email)
            .HasMaxLength(50)
            .IsRequired();

        builder
            .Property(e => e.FirstName)
            .HasMaxLength(50);

        builder
            .Property(e => e.LastName)
            .HasMaxLength(50);

        builder
            .HasMany(e => e.Articles)
            .WithOne(e => e.Author)
            .HasForeignKey(e => e.AuthorId);
        
        builder
            .HasMany(e => e.Comments)
            .WithOne(e => e.User)
            .HasForeignKey(e => e.UserId);
        
        builder
            .HasMany(e => e.OwnedCompanies)
            .WithOne(e => e.Owner)
            .HasForeignKey(e => e.OwnerId);
        
        builder
            .HasMany(e => e.Companies)
            .WithMany(e => e.Users)
            .UsingEntity<CompanyUser>(
                r => r
                    .HasOne(e => e.Company)
                    .WithMany(e => e.CompanyUsers)
                    .HasForeignKey(e => e.CompanyId),
                l => l
                    .HasOne(e => e.User)
                    .WithMany(e => e.CompanyUsers)
                    .HasForeignKey(e => e.UserId),
                b => b
                    .HasKey(e => e.Id)
            );

    }
}