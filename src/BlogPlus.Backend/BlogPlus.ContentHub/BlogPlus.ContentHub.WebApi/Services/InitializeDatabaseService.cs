﻿using BlogPlus.ContentHub.Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace BlogPlus.ContentHub.WebApi.Services;

internal sealed class InitializeDatabaseService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;

    public InitializeDatabaseService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceProvider.CreateScope();
        
        var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
        await dbContext.Database.MigrateAsync(cancellationToken: cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}