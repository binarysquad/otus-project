using BlogPlus.ContentHub.Application.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace BlogPlus.ContentHub.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ArticleController : ControllerBase
    {
        private readonly ILogger<ArticleController> _logger;
        private readonly IArticleService _articleService;

        public ArticleController(ILogger<ArticleController> logger, IArticleService articleService)
        {
            _logger = logger;
            _articleService = articleService;
        }

        /// <summary>
        /// Получить полную модель статью по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Полная модель статьи</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var article = await _articleService.GetFullArticleByIdAsync(id);
            
            return Ok(article);
        }
    }
}