namespace BlogPlus.Common.AspNetCore.Models;

public sealed record ValidationError
{
    public string Field { get; init; }
    
    public IReadOnlyList<string> ErrorMessages { get; init; }
}