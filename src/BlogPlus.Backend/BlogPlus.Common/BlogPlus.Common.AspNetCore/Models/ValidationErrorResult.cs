namespace BlogPlus.Common.AspNetCore.Models;

public sealed record ValidationErrorResult : ErrorResult
{
    public IEnumerable<ValidationError> ValidationErrors;
    
    public ValidationErrorResult()
    {
    }
    
    public ValidationErrorResult(IEnumerable<ValidationError> validationErrors)
    {
        ValidationErrors = validationErrors;
    }
}