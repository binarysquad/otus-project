using System.Net;
using Microsoft.AspNetCore.Http;
using BlogPlus.Common.AspNetCore.Exceptions;
using BlogPlus.Common.AspNetCore.Models;
using Newtonsoft.Json;

namespace BlogPlus.Common.AspNetCore.Middleware;

public sealed class ExceptionMiddleware
{
    private const string JsonContent = "application/json";
    
    private readonly RequestDelegate _next;

    public ExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (ApiException<ErrorResult> ex)
        {
            await WriteErrorResponse(httpContext, ex, httpContext.RequestAborted);
        }
        catch (ApiException<ValidationErrorResult> ex)
        {
            await WriteErrorResponse(httpContext, ex, httpContext.RequestAborted);
        }
        catch (Exception ex)
        {
            httpContext.Response.ContentType = JsonContent;
            httpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
    
            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorResult
            {
                StatusCode = (int) HttpStatusCode.InternalServerError,
                Message = "An error occurred on the server."
            }), httpContext.RequestAborted);
        }
    }

    private async Task WriteErrorResponse<T>(HttpContext context, ApiException<T> exception, CancellationToken cancellationToken) where T : ErrorResult, new()
    {
        context.Response.ContentType = JsonContent;
        context.Response.StatusCode = (int) exception.HttpStatusCode;

        exception.ErrorResult.StatusCode = (int)exception.HttpStatusCode;
        exception.ErrorResult.Message = exception.ErrorResult.Message;
    
        await context.Response.WriteAsync(JsonConvert.SerializeObject(exception.ErrorResult, Formatting.Indented, new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore
        }), cancellationToken);
    }
}