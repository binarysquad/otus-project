using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace BlogPlus.Common.AspNetCore.Options;

internal sealed class SwaggerUiVersioningOption : IConfigureNamedOptions<SwaggerUIOptions>
{
    private readonly IApiVersionDescriptionProvider _descriptionProvider;

    public SwaggerUiVersioningOption(IApiVersionDescriptionProvider descriptionProvider)
    {
        _descriptionProvider = descriptionProvider;
    }

    public void Configure(SwaggerUIOptions options)
    {
        foreach (var description in _descriptionProvider.ApiVersionDescriptions)
        {
            options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", "BlogPlus API Doc " + description.ApiVersion);
        }
    }

    public void Configure(string name, SwaggerUIOptions options)
    {
        Configure(options);
    }
}