using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace BlogPlus.Common.AspNetCore.Extensions;

public static class HttpContextExtensions
{
    public static string GetAccessToken(this HttpContext context)
    {
        return context.Request.Headers[HeaderNames.Authorization]
            .ToString()
            .Replace("Bearer ", string.Empty);
    }
}