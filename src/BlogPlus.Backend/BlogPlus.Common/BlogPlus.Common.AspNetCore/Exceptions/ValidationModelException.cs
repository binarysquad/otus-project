using System.Net;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public sealed class ValidationModelException : ApiException<ValidationErrorResult>
{
    internal override HttpStatusCode HttpStatusCode => HttpStatusCode.BadRequest;
    
    public ValidationModelException(string message) : base(message)
    {
        ErrorResult = new ValidationErrorResult
        {
            Message = message,
            StatusCode = (int)HttpStatusCode
        };
    }

    public ValidationModelException(string message, IDictionary<string, string[]> validationErrorResult): base(message)
    {
        ErrorResult = new ValidationErrorResult(validationErrorResult.Select(kv => new ValidationError
        {
            Field = kv.Key,
            ErrorMessages = kv.Value
        }))
        {
            StatusCode = (int)HttpStatusCode,
            Message = message
        };
    }
}