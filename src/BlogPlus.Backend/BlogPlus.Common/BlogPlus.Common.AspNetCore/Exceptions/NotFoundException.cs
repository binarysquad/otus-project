using System.Net;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public sealed class NotFoundException : ApiException<ErrorResult>
{
    internal override HttpStatusCode HttpStatusCode => HttpStatusCode.NotFound;
    
    public NotFoundException(string message): base(message) { }
}