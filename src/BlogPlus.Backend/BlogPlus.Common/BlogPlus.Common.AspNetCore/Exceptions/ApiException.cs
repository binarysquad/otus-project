using System.Net;
using System.Runtime.Serialization;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public abstract class ApiException<T> : Exception where T : ErrorResult, new()
{
    internal abstract HttpStatusCode HttpStatusCode { get; }

    public T ErrorResult { get; protected init; } = new();

    protected ApiException() { }

    protected ApiException(string message)
    {
         ErrorResult = new T
         {
            StatusCode = (int) HttpStatusCode,
            Message = message
         };
    }

    protected ApiException(string message, Exception innerException)
    {
    }
    
    protected ApiException(SerializationInfo info, StreamingContext context)
    {
    }
}