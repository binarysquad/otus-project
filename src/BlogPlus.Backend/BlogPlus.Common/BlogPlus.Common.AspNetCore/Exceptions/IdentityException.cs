using System.Net;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public sealed class IdentityException : ApiException<ErrorResult>
{
    internal override HttpStatusCode HttpStatusCode => HttpStatusCode.InternalServerError;

    public IdentityException(string message): base(message) { }
}