﻿using System.Net;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public sealed class AlreadyExistsException : ApiException<ErrorResult>
{
    internal override HttpStatusCode HttpStatusCode => HttpStatusCode.Conflict;
    
    public AlreadyExistsException(string message): base(message) { }
}