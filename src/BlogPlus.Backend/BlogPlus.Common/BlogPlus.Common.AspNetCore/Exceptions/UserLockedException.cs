﻿using System.Net;
using BlogPlus.Common.AspNetCore.Models;

namespace BlogPlus.Common.AspNetCore.Exceptions;

public class UserLockedException : ApiException<ErrorResult>
{
    internal override HttpStatusCode HttpStatusCode => HttpStatusCode.BadRequest;
    
    public UserLockedException(string message): base(message) { }
}