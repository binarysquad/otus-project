using BlogPlus.Common.Caching.CacheStore;
using BlogPlus.Common.Caching.CacheStore.Implementation;
using BlogPlus.Common.Caching.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace BlogPlus.Common.Caching.Configuration;

public static class DistributedCacheConfiguration
{
    public static IServiceCollection AddRedisCache(this IServiceCollection services)
    {
        services
            .AddOptions<RedisOptions>()
            .BindConfiguration(nameof(RedisOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();
        
        services.AddStackExchangeRedisCache(options =>
        {
            using var scope = services.BuildServiceProvider().CreateScope();
            var redisOptions = scope.ServiceProvider.GetRequiredService<IOptions<RedisOptions>>();
            
            options.Configuration = redisOptions.Value.ConnectionString;
            options.InstanceName = redisOptions.Value.InstanceName;
        });
        
        services.AddTransient<IDeactivatedTokenStore, DeactivatedTokenStore>();
        services.AddHttpContextAccessor();
        
        return services;
    }
}