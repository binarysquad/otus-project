namespace BlogPlus.Common.Caching.CacheStore;

public interface IDeactivatedTokenStore
{
    Task<bool> IsActiveAsync(string accessToken);
    
    Task DeactivateAsync(string accessToken);
}