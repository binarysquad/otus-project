using BlogPlus.Common.Authorization.Options;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;

namespace BlogPlus.Common.Caching.CacheStore.Implementation;

public sealed class DeactivatedTokenStore : IDeactivatedTokenStore
{
    private readonly IDistributedCache _cache;
    private readonly IOptions<JwtOptions> _jwtOptions;

    public DeactivatedTokenStore(IDistributedCache cache, IOptions<JwtOptions> jwtOptions)
    {
        _cache = cache;
        _jwtOptions = jwtOptions;
    }

    public async Task<bool> IsActiveAsync(string token)
    {
        return await _cache.GetStringAsync(GetKey(token)) == null;
    }

    public async Task DeactivateAsync(string token)
    {
        await _cache.SetStringAsync(GetKey(token)," ", new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = _jwtOptions.Value.ExpiredPeriod
            });
    }

    private static string GetKey(string token)
        => $"tokens:{token}:deactivated";
}