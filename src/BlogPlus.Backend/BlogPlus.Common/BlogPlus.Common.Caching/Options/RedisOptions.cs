using System.ComponentModel.DataAnnotations;

namespace BlogPlus.Common.Caching.Options;

public sealed record RedisOptions
{
    [Required]
    public string ConnectionString { get; init; }
    
    [Required]
    public string InstanceName { get; init; }
}