using System.ComponentModel.DataAnnotations;

namespace BlogPlus.Common.Authorization.Options;

public sealed record JwtOptions
{
    [Required]
    public string Key { get; init; }
    
    [Required]
    public string Issuer { get; init; }
    
    [Required]
    public string Audience { get; init; }
    
    public TimeSpan ExpiredPeriod { get; init; }
}