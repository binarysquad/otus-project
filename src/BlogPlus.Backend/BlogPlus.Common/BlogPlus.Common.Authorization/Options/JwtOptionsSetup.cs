using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BlogPlus.Common.Authorization.Options;

internal sealed class JwtOptionsSetup : IConfigureNamedOptions<JwtBearerOptions>
{
    private readonly JwtOptions _jwtOptions;

    public JwtOptionsSetup(IOptions<JwtOptions> jwtOptions)
    {
        _jwtOptions = jwtOptions.Value;
    }

    public void Configure(JwtBearerOptions options)
    {
        var key = Encoding.UTF8.GetBytes(_jwtOptions.Key);

        options.SaveToken = true;
        options.MapInboundClaims = false;
        options.IncludeErrorDetails = true;
        
        options.TokenValidationParameters = new TokenValidationParameters
        {
            IssuerSigningKey = new SymmetricSecurityKey(key),
            
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidIssuer = _jwtOptions.Issuer,
            
            ValidateAudience = false,
            ValidAudience = _jwtOptions.Audience,
            
            RequireExpirationTime = false,
            ValidateLifetime = true
        };
    }

    public void Configure(string? name, JwtBearerOptions options)
    {
        Configure(options);
    }
}