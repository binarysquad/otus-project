namespace BlogPlus.ApiGateway.ReverseProxy.Options;

public sealed record IdentityUserRateLimitOptions : TokenBucketLimitOptions;