using System.ComponentModel.DataAnnotations;

namespace BlogPlus.ApiGateway.ReverseProxy.Options;

public record TokenBucketLimitOptions
{
    [Required]
    public int TokenLimit { get; init; }
    
    [Required]
    public int QueueLimit { get; init; }
    
    [Required]
    public int ReplenishmentPeriodInSeconds { get; init; }
    
    [Required]
    public int TokensPerPeriod { get; init; }
}