namespace BlogPlus.ApiGateway.ReverseProxy.Options;

public sealed record AnonymousRateLimitOptions : TokenBucketLimitOptions;