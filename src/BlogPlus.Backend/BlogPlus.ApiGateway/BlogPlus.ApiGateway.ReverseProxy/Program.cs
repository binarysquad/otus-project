using BlogPlus.ApiGateway.ReverseProxy.Configuration;
using BlogPlus.ApiGateway.ReverseProxy.Middleware;
using BlogPlus.Common.Caching.Configuration;
using Microsoft.Extensions.Options;
using Yarp.ReverseProxy.Swagger;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRateLimiting(builder.Configuration);
builder.Services.AddRedisCache();
builder.Services.AddConfiguredSwagger();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        var config = app.Services.GetRequiredService<IOptionsMonitor<ReverseProxyDocumentFilterConfig>>().CurrentValue;
        foreach (var cluster in config.Clusters)
            c.SwaggerEndpoint($"/swagger/{cluster.Key}/swagger.json", cluster.Key);
    });
}

app.UseHttpsRedirection();
app.UseRateLimiter();
app.UseMiddleware<TokenValidationMiddleware>();
app.MapReverseProxy();
app.Run();