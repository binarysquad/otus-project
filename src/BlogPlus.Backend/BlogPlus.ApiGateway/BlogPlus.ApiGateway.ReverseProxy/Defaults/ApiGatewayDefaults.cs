namespace BlogPlus.ApiGateway.ReverseProxy.Defaults;

internal static class ApiGatewayDefaults
{
    internal const string ReverseProxySection = "ReverseProxy";
}