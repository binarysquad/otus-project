using System.Net;
using BlogPlus.Common.AspNetCore.Extensions;
using BlogPlus.Common.AspNetCore.Models;
using BlogPlus.Common.Caching.CacheStore;
using Newtonsoft.Json;

namespace BlogPlus.ApiGateway.ReverseProxy.Middleware;

internal sealed class TokenValidationMiddleware
{
    private readonly IDeactivatedTokenStore _deactivatedTokenStore;
    private readonly RequestDelegate _next;

    public TokenValidationMiddleware(RequestDelegate next, IDeactivatedTokenStore deactivatedTokenStore)
    {
        _next = next;
        _deactivatedTokenStore = deactivatedTokenStore;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var accessToken = context.GetAccessToken();

        if (!string.IsNullOrWhiteSpace(accessToken) && !await _deactivatedTokenStore.IsActiveAsync(accessToken))
        {
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.ContentType = "application/json";
            
            await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorResult
            {
                StatusCode = (int) HttpStatusCode.Unauthorized,
                Message = "Invalid access token."
            }), context.RequestAborted);
            return;
        }
        
        await _next(context);
    }
}