using BlogPlus.ApiGateway.ReverseProxy.Swagger;
using Yarp.ReverseProxy.Swagger;

namespace BlogPlus.ApiGateway.ReverseProxy.Configuration;

internal static class SwaggerConfiguration
{
    internal static IServiceCollection AddConfiguredSwagger(this IServiceCollection services)
    {
        services.ConfigureOptions<ConfigureSwaggerOptions>();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            options.DocumentFilter<ReverseProxyDocumentFilter>();
        });
        
        return services;
    }
}