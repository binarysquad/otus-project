using System.Net;
using System.Threading.RateLimiting;
using BlogPlus.ApiGateway.ReverseProxy.Defaults;
using BlogPlus.ApiGateway.ReverseProxy.Options;
using BlogPlus.Common.AspNetCore.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Yarp.ReverseProxy.Swagger.Extensions;

namespace BlogPlus.ApiGateway.ReverseProxy.Configuration;

internal static class RateLimitConfiguration
{
    internal static IServiceCollection AddRateLimiting(this IServiceCollection services, IConfiguration configuration)
    {
        var rateLimitConfiguration = configuration.GetSection(ApiGatewayDefaults.ReverseProxySection);
        
        services
            .AddOptions<IdentityUserRateLimitOptions>()
            .BindConfiguration(nameof(IdentityUserRateLimitOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();

        services
            .AddOptions<AnonymousRateLimitOptions>()
            .BindConfiguration(nameof(AnonymousRateLimitOptions))
            .ValidateDataAnnotations()
            .ValidateOnStart();

        services.AddRateLimiter(limiterOptions =>
        {
            using var scope = services.BuildServiceProvider().CreateScope();
            var identityUserRateLimit = scope.ServiceProvider.GetRequiredService<IOptions<IdentityUserRateLimitOptions>>();
            var anonymousRateLimit = scope.ServiceProvider.GetRequiredService<IOptions<AnonymousRateLimitOptions>>();
            
            limiterOptions.RejectionStatusCode = StatusCodes.Status429TooManyRequests;
            limiterOptions.GlobalLimiter = PartitionedRateLimiter.Create<HttpContext, string>(context =>
            {
                var accessToken = context.GetAccessToken();
                if (!StringValues.IsNullOrEmpty(accessToken))
                {
                    return RateLimitPartition.GetTokenBucketLimiter(accessToken, _ =>
                        new TokenBucketRateLimiterOptions
                        {
                            TokenLimit = identityUserRateLimit.Value.TokenLimit,
                            QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
                            QueueLimit = identityUserRateLimit.Value.QueueLimit,
                            ReplenishmentPeriod = TimeSpan.FromSeconds(identityUserRateLimit.Value.ReplenishmentPeriodInSeconds),
                            TokensPerPeriod = identityUserRateLimit.Value.TokensPerPeriod,
                            AutoReplenishment = true
                        });
                }

                IPAddress? remoteIpAddress = context.Connection.RemoteIpAddress;
                if (!IPAddress.IsLoopback(remoteIpAddress!))
                {
                    return RateLimitPartition.GetTokenBucketLimiter
                    (remoteIpAddress?.ToString()!, _ =>
                        new TokenBucketRateLimiterOptions
                        {
                            TokenLimit = anonymousRateLimit.Value.TokenLimit,
                            QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
                            QueueLimit = anonymousRateLimit.Value.QueueLimit,
                            ReplenishmentPeriod = TimeSpan.FromSeconds(anonymousRateLimit.Value.ReplenishmentPeriodInSeconds),
                            TokensPerPeriod = anonymousRateLimit.Value.TokensPerPeriod,
                            AutoReplenishment = true
                        });
                }

                return RateLimitPartition.GetNoLimiter(IPAddress.Loopback.ToString());
            });
        });
        
        services
            .AddReverseProxy()
            .LoadFromConfig(rateLimitConfiguration)
            .AddSwagger(rateLimitConfiguration);

        return services;
    }
}