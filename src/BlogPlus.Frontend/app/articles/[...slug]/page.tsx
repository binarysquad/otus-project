import Image from "next/image";
import React from "react";
async function getData(articleId: string) {
  const res = await fetch(
    `https://blogplus.cyclic.cloud/articles/${articleId}`
  );
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}
export default async function Page({ params }: { params: { slug: string } }) {
  const article = await getData(params.slug);

  return (
    <div className="container">
      <div className="text-center space-y-3 mb-10">
        <Image
          src={`/images/${article.thumbnail}`}
          width={500}
          height={200}
          alt=""
          className="mx-auto mb-10"
        />
        <h1 className="text-3xl font-medium">{article.title}</h1>
        <p>{article.publication_date}</p>
      </div>
      <div>
        <p>{article.body}</p>
      </div>
    </div>
  );
}
