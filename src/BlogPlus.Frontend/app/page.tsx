import Image from "next/image";
import FeedPost from "./Components/FeedPost";
import { useEffect, useState } from "react";

async function getData() {
  const res = await fetch("https://blogplus.cyclic.cloud/articles");
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data");
  }

  return res.json();
}

export default async function Home() {
  const articles = await getData();
  console.log(articles);

  return (
    <>
      <div className="bg-white rounded-xl border border-gray-100 p-8">
        <h1 className="text-3xl font-bold text-center mb-3">
          Community Voices: Where Ideas Thrive
        </h1>
        <p className="text-center text-sm leading-6">
          Join a vibrant community of thinkers, innovators, and storytellers.
          <br />
          Whether you&rsquo;re an individual sharing your unique perspective or
          a company connecting with your audience, <br />
          Community Voices is your space to express, engage, and inspire.
        </p>
      </div>
      {articles.map((article: Article) => {
        return <FeedPost key={article.id} article={article} />;
      })}
    </>
  );
}
