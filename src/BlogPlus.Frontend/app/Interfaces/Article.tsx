interface Article {
  id: number;
  title: string;
  description: string;
  body: string;
  tags: string[];
  thumbnail: string;
  author: Author;
  publication_date: string;
}
