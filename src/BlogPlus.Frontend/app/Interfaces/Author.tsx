interface Author {
  first_name: string;
  last_name: string;
  profile_image: string;
}
