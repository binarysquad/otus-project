import React from "react";
import Image from "next/image";
import Link from "next/link";

export default function FeedPost({ article }: { article: Article }) {
  const apiServer = "http://localhost:3000";
  return (
    <div className="p-8 border border-gray-100 rounded-xl space-y-5 bg-white">
      <div className="flex items-center justify-between ">
        <div className="flex items-center gap-2">
          <span className="w-[40px] h-[40px] rounded-full border flex justify-center items-center bg-blue-500 text-white">
            {article.author.profile_image ? (
              <Image
                src={`/images/${article.author.profile_image}`}
                className="max-w-[35px]"
                height={35}
                width={35}
                alt="Profile pic"
              />
            ) : (
              <span className="text-xl">{article.author.first_name[0]}</span>
            )}
          </span>
          <p>
            {article.author.first_name} {article.author.last_name}
          </p>
        </div>
        <span> {article.publication_date}</span>
      </div>
      <div className="flex gap-2">
        <div>
          <h2 className="font-bold text-2xl mb-2">
            <Link
              href={`/articles/${article.id}`}
              className="transition hover:text-pink-500"
            >
              {article.title}
            </Link>
          </h2>

          <p>
            <Link href="#">{article.description}</Link>
          </p>
        </div>
        <div>
          <Image
            src={`/images/${article.thumbnail}`}
            className="max-w-[200px]"
            height={200}
            width={200}
            alt="Post pic"
          />
        </div>
      </div>
      <div className="flex items-center gap-2 justify-between">
        <ul className="flex gap-2 text-sm text-gray-500">
          <li className="flex gap-1">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
              />
            </svg>
            20
          </li>
          <li className="flex gap-1">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M7.5 8.25h9m-9 3H12m-9.75 1.51c0 1.6 1.123 2.994 2.707 3.227 1.129.166 2.27.293 3.423.379.35.026.67.21.865.501L12 21l2.755-4.133a1.14 1.14 0 01.865-.501 48.172 48.172 0 003.423-.379c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"
              />
            </svg>
            3
          </li>
          <li className="flex gap-1">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
              />
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
              />
            </svg>
            200 reads
          </li>
        </ul>
        {/* tags */}
        <ul className="flex gap-2">
          {article.tags.map((tag) => {
            return (
              <li key={tag}>
                <a
                  href="#"
                  className="border border-blue-200 bg-blue-50 transition  rounded-md p-1 px-2 px- text-xs hover:bg-blue-500 hover:text-white"
                >
                  {tag}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}
