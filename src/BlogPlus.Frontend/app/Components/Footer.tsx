import React from "react";

export default function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="text-center">© 2023 Blog+</div>
      </div>
    </footer>
  );
}
