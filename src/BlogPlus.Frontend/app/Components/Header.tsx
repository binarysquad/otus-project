import Image from "next/image";
import Link from "next/link";
import React from "react";

export default function Header() {
  return (
    <header className="p-4 border-b bg-white">
      <div className="container ">
        <nav className="flex justify-between items-center">
          <span className="font-bold text-xl text-pink-600">
            <Link href="/">
              <Image src="/logo.svg" width={80} height={50} alt="logo" />
            </Link>
          </span>
          <ul className="flex space-x-4 font-medium">
            <li>
              <a href="#" className="hover:text-pink-500 transition">
                My Feed
              </a>
            </li>
            <li>
              <a href="#" className="hover:text-pink-500 transition">
                Explore
              </a>
            </li>
            <li>
              <a href="#" className="hover:text-pink-500 transition">
                Tags
              </a>
            </li>
          </ul>
          <div className="flex items-center  gap-4">
            <a
              href="#"
              className="bg-pink-600 border border-pink-700 text-white flex items-center p-2 px-4 gap-1 rounded-full"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-4 h-4"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"
                />
              </svg>
              Write
            </a>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 cursor-pointer hover:stroke-pink-600 transition"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0"
              />
            </svg>

            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6 cursor-pointer hover:stroke-pink-600 transition"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
              />
            </svg>
          </div>
        </nav>
      </div>
    </header>
  );
}
