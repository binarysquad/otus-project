# BlogPlus

## Как запустить

1. Поднять окружение
2. Поднять микросервисы

### 1 Поднять окружение

Сначала запускается окружение, например, базы данных, так как микросервисы при запуске пытаются к ним подключиться, и
если соответствующее окружение не запущено, то микросервис при запуске выдаст ошибку.

1. Добавить env-файл при необходимости
2. Запустить через docker-compose

#### Как добавить env-файл

Для контейнеров окружения прописаны env-файлы (см. сервисы в docker-compose.infrastructure.yml), их следует
поместить рядом с файлом docker-compose.infrastructure.yml.

Для docker-сервиса contenthub_postgres_db необходим env-файл **contenthub_postgres_db.env** со следующими переменными:
- POSTGRES_DB=<навзание бд>
- POSTGRES_USER=<имя пользователя>
- POSTGRES_PASSWORD=<пароль пользователя>

Для docker-сервиса authorization_postgres_db необходим env-файл **authorization_postgres_db.env** со следующими переменными:
- POSTGRES_DB=<навзание бд>
- POSTGRES_USER=<имя пользователя>
- POSTGRES_PASSWORD=<пароль пользователя>

Для docker-сервиса pgadmin необходим env-файл **pgadmin.env** со следующими переменными:
- PGADMIN_DEFAULT_EMAIL=<имя пользователя>
- PGADMIN_DEFAULT_PASSWORD=<пароль пользователя>

#### Как запустить через docker-compose

Поднять окружение (базы данных, админки, и тд) в докере можно с помощью команды:
**docker compose -f docker-compose.infrastructure.yml up -d**

### 2 Поднять микросервисы

#### 2.1 Через Docker

1. Собрать image
2. Добавить env-файл при необходимости
3. Запустить через docker-compose

##### Как собрать image

Перед тем, как запустить команду docker compose, необходимо собрать соответствующие image для микросервисов.

Для сборки image для микросервиса ContentHub необходимо запустить команду в папке BlogPlus.ContentHub:
**docker build -t blogplus/contenthub -f .\BlogPlus.ContentHub.WebApi\Dockerfile .**

##### Как добавить env-файл

Для сервисов в docker-compose файле, так же прописаны env-файлы, которые необходимо расположить рядом с docker-compose файлом.

Для docker-сервиса contenthub необходим env-файл **contenthub.env** со следующими переменными:
- ASPNETCORE_ENVIRONMENT=<название окружения, например, Development>
- ConnectionStrings__PostgresDb=<connectionstring к базе данных PostrgeSQL (конктейнер contenthub_postgres_db)>

##### Как запустить через docker-compose

Микросервисы прописаны в файле docker-compose.services.yml и поднимаются с помощью команды:
**docker compose -f docker-compose.services.yml up -d**

Чтобы запустить один или несколько микросервисов надо прописать названия соответствующих контейнеров после основной команды через пробел:
docker compose -f docker-compose.services.yml up -d **contenthub** **<еще одно имя контейнера>**

#### 2.1 Через IDE

Чтобы запустить проекты через IDE необходимо добавить чувствительне переменные окружениz, типа подключений к БД, указанные в env-файлах,
с помощью user secrets.

#### 